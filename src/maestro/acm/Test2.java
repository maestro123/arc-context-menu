package maestro.acm;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Artyom.Borovsky on 07.10.2015.
 */
public class Test2 extends Activity {

    private ArcContextMenu mArcMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test2);

        Button test = (Button) findViewById(R.id.click_button);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Click :)", Toast.LENGTH_SHORT).show();
            }
        });

        mArcMenu = (ArcContextMenu) findViewById(R.id.arc_menu);
        mArcMenu.setCenterDrawable(getResources().getDrawable(R.drawable.center_circle));
        mArcMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test").id(0).icon(getResources().getDrawable(R.drawable.circle1)));
        mArcMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test 2").id(1).icon(getResources().getDrawable(R.drawable.circle2)));
        mArcMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test 3").id(2).icon(getResources().getDrawable(R.drawable.circle3)));

        test.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mArcMenu.show();
                return false;
            }
        });

    }
}