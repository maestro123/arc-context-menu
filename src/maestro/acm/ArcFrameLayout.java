package maestro.acm;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * Created by Artyom.Borovsky on 07.10.2015.
 */
public class ArcFrameLayout extends FrameLayout {

    public static final String TAG = ArcFrameLayout.class.getSimpleName();

    private ArcContextMenu mMenu;

    public ArcFrameLayout(Context context) {
        super(context);
    }

    public ArcFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArcFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mMenu != null && mMenu.dispatchTouchEvent(ev)) {
            return false;
        }
        return super.dispatchTouchEvent(ev);
    }

    public void setArcMenu(ArcContextMenu contextMenu) {
        mMenu = contextMenu;
    }

}
