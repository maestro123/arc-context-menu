package maestro.acm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Maine extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button test = (Button) findViewById(R.id.test);
        Button test2 = (Button) findViewById(R.id.test2);
        Button test3 = (Button) findViewById(R.id.test3);
        Button test4 = (Button) findViewById(R.id.test4);
        Button test5 = (Button) findViewById(R.id.test5);
        Button test6 = (Button) findViewById(R.id.test6);
        Button test7 = (Button) findViewById(R.id.test7);

        test.setOnLongClickListener(mLongClickListener);
        test2.setOnLongClickListener(mLongClickListener);
        test3.setOnLongClickListener(mLongClickListener);
        test4.setOnLongClickListener(mLongClickListener);
        test5.setOnLongClickListener(mLongClickListener);
        test6.setOnLongClickListener(mLongClickListener);
        test7.setOnLongClickListener(mLongClickListener);

        test.setOnClickListener(mClickListener);
        test2.setOnClickListener(mClickListener);
        test3.setOnClickListener(mClickListener);
        test4.setOnClickListener(mClickListener);
        test5.setOnClickListener(mClickListener);
        test6.setOnClickListener(mClickListener);
        test7.setOnClickListener(mClickListener);

        Button clickButton = (Button) findViewById(R.id.test_click);
        clickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Clicked!!!", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.startTest2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), Test2.class));
            }
        });

    }

    View.OnLongClickListener mLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            ArcContextMenu contextMenu = new ArcContextMenu(getBaseContext());
            contextMenu.setCenterDrawable(getResources().getDrawable(R.drawable.center_circle));
            contextMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test").id(0).icon(getResources().getDrawable(R.drawable.circle1)));
            contextMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test 2").id(1).icon(getResources().getDrawable(R.drawable.circle2)));
            contextMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test 3").id(2).icon(getResources().getDrawable(R.drawable.circle3)));
            contextMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test 4").id(3).icon(getResources().getDrawable(R.drawable.circle4)));
            contextMenu.addMenuItem(new ArcContextMenu.ArcMenuItem().title("Test 5").id(4).icon(getResources().getDrawable(R.drawable.circle5)));
            contextMenu.add(Maine.this);
            contextMenu.show();
            return true;
        }
    };

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            contextMenu.show();
        }
    };

}
