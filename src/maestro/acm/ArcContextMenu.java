package maestro.acm;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.*;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.*;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Artyom.Borovsky on 07.10.2015.
 */

public class ArcContextMenu extends ViewGroup {

    public static final String TAG = ArcContextMenu.class.getSimpleName();

    private static final float START_DEGREES = 180f;
    private static final float STEP_DEGREES = 45f;
    private static final float SCALE = 1.3f;

    private ArrayList<ArcMenuItem> menuItems = new ArrayList<ArcMenuItem>();

    private ArrayList<ArcHighlightRect> mHighlightBounds;
    private Paint mHighlightPaint;

    private MotionEvent mLastEvent;
    private Drawable mCenterDrawable;
    private OnArcMenuItemSelectListener mListener;

    private int centerX;
    private int centerY;

    private boolean isVisible;
    private boolean isConfigured;
    private boolean isActualAttached;
    private boolean computeByMaxSize = true;
    private boolean confirmOnClick = false;

    public interface OnArcMenuItemSelectListener {
        void onArcMenuItemSelect(ArcMenuItem item);
    }

    public ArcContextMenu(Context context) {
        super(context);
        init(null);
    }

    public ArcContextMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ArcContextMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        isActualAttached = true;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        isActualAttached = false;
    }

    private final void init(AttributeSet attrs) {
        ColorDrawable mDrawable = new ColorDrawable(Color.BLACK);
        mDrawable.setAlpha(0);
        setBackgroundDrawable(mDrawable);
        mHighlightPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHighlightPaint.setStyle(Paint.Style.STROKE);
        mHighlightPaint.setStrokeWidth(Math.max(2, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics())));

        boolean highlightColorSet = false;
        if (attrs != null) {
            int[] att = new int[]{R.attr.arcMenuCenterDrawable, R.attr.arcMenuHighlightColor};
            TypedArray atArray = getContext().obtainStyledAttributes(attrs, att);
            mCenterDrawable = atArray.getDrawable(0);
            if (atArray.hasValue(1)) {
                mHighlightPaint.setColor(atArray.getColor(1, 0));
                highlightColorSet = true;
            }
        }

        if (mCenterDrawable == null) {
            TypedValue value = new TypedValue();
            getContext().getTheme().resolveAttribute(R.attr.arcMenuCenterDrawable, value, true);
            if (value.resourceId > 0) {
                mCenterDrawable = getResources().getDrawable(value.resourceId);
            }
        }

        if (!highlightColorSet) {
            TypedValue value = new TypedValue();
            getContext().getTheme().resolveAttribute(R.attr.arcMenuHighlightColor, value, true);
            mHighlightPaint.setColor(value.data);
        }

    }

    public void setOnArcMenuItemSelectListener(OnArcMenuItemSelectListener listener) {
        mListener = listener;
    }

    public void setHighlightAlpha(int alpha) {
        mHighlightPaint.setAlpha(alpha);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mCenterDrawable != null) {
            canvas.save();
            int drawableWidth = mCenterDrawable.getIntrinsicWidth();
            int drawableHeight = mCenterDrawable.getIntrinsicHeight();
            canvas.translate(centerX - drawableWidth / 2, centerY - drawableHeight / 2);
            mCenterDrawable.setBounds(0, 0, drawableWidth, drawableHeight);
            mCenterDrawable.draw(canvas);
            canvas.restore();
        }

        if (mHighlightBounds != null) {
            for (ArcHighlightRect rect : mHighlightBounds) {
                rect.draw(canvas, mHighlightPaint);
            }
        }

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (mLastEvent == null) {
            return;
        }
        obtainCenterAndBounds();
        isConfigured = true;
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //TODO: add some restore logic
        dismiss();
    }

    public void setHighlightBounds(ArrayList<ArcHighlightRect> mBounds) {
        mHighlightBounds = mBounds;
        if (isVisible) {
            invalidate();
        }
    }

    public void setLastMotionEvent(MotionEvent event) {
        mLastEvent = event;
    }

    private void obtainCenterAndBounds() {
        if (mLastEvent == null || menuItems.size() == 0) {
            return;
        }

        final int childCount = getChildCount();

        centerX = (int) mLastEvent.getRawX();
        centerY = (int) mLastEvent.getRawY();
        float degrees = START_DEGREES;
        float countAdjustDegrees = 0;
        boolean reverse = false;

        if (childCount > 4) {
            degrees -= (countAdjustDegrees = (childCount - 4) * STEP_DEGREES);
        }

        prepareBounds(degrees, reverse);
        Rect bounds = getArcBounds();
        if (bounds.left < 0 || bounds.right > getWidth()) {
            float wMove = .5f - (float) centerX / getWidth();
            float wDegrees = (centerX < getWidth() / 2 ? 225f + countAdjustDegrees : 45f + countAdjustDegrees) * wMove;
            degrees += wDegrees;
            prepareBounds(degrees, reverse);
            bounds = getArcBounds();
            if (bounds.right > getWidth() && (bounds.bottom <= getHeight() - (bounds.right - getWidth()))) {
                degrees += (float) bounds.right / getWidth() * 360f;
                prepareBounds(degrees, reverse);
                bounds = getArcBounds();
            }
        }

        if (bounds.top < 0 || bounds.bottom > getHeight()) {
            float hMove = .5f - (float) centerY / getHeight();
            float hDegrees = (centerY < getHeight() / 2
                    ? (centerX < getWidth() / 2 ? -180f : -180f) - countAdjustDegrees
                    : (centerX < getWidth() / 2 ? 15f : -45f) - countAdjustDegrees)
                    * hMove;
            degrees += hDegrees;
            prepareBounds(degrees, reverse);
            bounds = getArcBounds();
            if (bounds.top < 0 && bounds.right <= (getWidth() + bounds.top)) {
                degrees += (float) Math.abs(bounds.top) / getHeight() * -360f;
                prepareBounds(degrees, reverse);
            } else if (bounds.bottom > getHeight() && bounds.right <= (getWidth() - (bounds.bottom - getHeight()))) {
                degrees += (float) bounds.bottom / getHeight() * 360f;
                prepareBounds(degrees, reverse);
            }
            bounds = getArcBounds();
            if (bounds.right > getWidth() && (bounds.bottom <= getHeight() - (bounds.right - getWidth()))) {
                degrees += (float) bounds.right / getWidth() * -360f;
                prepareBounds(degrees, reverse);
            }
        }

        for (int i = 0; i < childCount; i++) {
            layoutChild(i);
        }
    }

    private final float reverseDegree(float degree, float increase) {
        float outDegree = degree + increase;
        if (outDegree > 360) {
            return outDegree - 360;
        } else if (outDegree < 0) {
            return degree - 360 + increase;
        }
        return outDegree;
    }

    private Rect getArcBounds() {
        int maxLeft = 0;
        int maxRight = 0;
        int maxTop = 0;
        int maxBottom = 0;

        for (ArcMenuItem item : menuItems) {
            maxLeft = Math.min(item.bounds.left, maxLeft);
            maxRight = Math.max(item.bounds.right, maxRight);
            maxTop = Math.min(item.bounds.top, maxTop);
            maxBottom = Math.max(item.bounds.bottom, maxBottom);
        }
        return new Rect(maxLeft, maxTop, maxRight, maxBottom);
    }

    private final void prepareBounds(float startDegrees, boolean reverse) {
        final int childCount = getChildCount();
        if (reverse) {
            for (int i = childCount - 1; i > -1; i--) {
                prepareViewBounds(i, centerX, centerY, startDegrees);
                startDegrees += STEP_DEGREES;
            }
        } else {
            for (int i = 0; i < childCount; i++) {
                prepareViewBounds(i, centerX, centerY, startDegrees);
                startDegrees += STEP_DEGREES;
            }
        }
    }

    private final void prepareViewBounds(int position, int cX, int cY, float degrees) {
        final View child = getChildAt(position);
        final int childWidth = child.getMeasuredWidth();
        final int childHeight = child.getMeasuredHeight();
        final Rect bounds;
        if (computeByMaxSize) {
            final int maxSize = getScaledSize(Math.max(childWidth, childHeight));
            final int radius = computeRadius(getChildCount(), maxSize, 0, 0);
            bounds = computeChildFrame(cX, cY, radius, degrees, maxSize);
        } else {
            final int maxWidth = getScaledSize(childWidth);
            final int maxHeight = getScaledSize(childHeight);
            final int radiusWidth = computeRadius(getChildCount(), maxWidth, 0, 0);
            final int radiusHeight = computeRadius(getChildCount(), maxHeight, 0, 0);
            bounds = computeChildFrame(cX, cY, radiusWidth, radiusHeight, degrees, maxWidth, maxHeight);
        }
        getMenuItem(child.getId()).bounds = bounds;
    }

    private int getScaledSize(float size) {
        if (SCALE > 1f) {
            return (int) (size * (1f + (SCALE - 1f) / 2));
        }
        return (int) size;
    }

    private void layoutChild(int position) {
        final View child = getChildAt(position);
        final ArcMenuItem item = getMenuItem(child.getId());
        final Rect bounds = item.bounds;
        child.layout(bounds.left, bounds.top, bounds.right, bounds.bottom);
    }

    private int computeRadius(final int childCount, final int childSize, final int childPadding, final int minRadius) {
        if (childCount < 2) {
            return minRadius;
        }
        final float perHalfDegrees = STEP_DEGREES / 2;
        final int perSize = childSize + childPadding;
        final int radius = (int) ((perSize / 2) / Math.sin(Math.toRadians(perHalfDegrees)));
        return Math.max(radius, minRadius);
    }

    private Rect computeChildFrame(final int centerX, final int centerY, final int radius, final float degrees, final int size) {
        return computeChildFrame(centerX, centerY, radius, radius, degrees, size, size);
    }

    private Rect computeChildFrame(final int centerX, final int centerY, final int radiusW, final int radiusH,
                                   final float degrees, final int width, final int height) {
        final double childCenterX = centerX + radiusW * Math.cos(Math.toRadians(degrees));
        final double childCenterY = centerY + radiusH * Math.sin(Math.toRadians(degrees));
        return new Rect((int) (childCenterX - width / 2), (int) (childCenterY - height / 2),
                (int) (childCenterX + width / 2), (int) (childCenterY + height / 2));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
            }
        }
    }

    public void addMenuItem(ArcMenuItem item) {
        if (menuItems.size() == 8) {
            throw new IndexOutOfBoundsException("ArcContextMenu.class support only 8 menu items");
        }
        menuItems.add(item);
        addView(makeItemView(item));
    }

    private View makeItemView(ArcMenuItem item) {
        View v = inflate(getContext(), R.layout.arc_menu_item_view, null);
        TextView textView = (TextView) v.findViewById(R.id.title);
        ImageView imageView = (ImageView) v.findViewById(R.id.image);
        if (item.titleResource != -1) {
            textView.setText(item.titleResource);
        } else {
            textView.setText(item.title);
        }
        imageView.setImageDrawable(item.icon);
        v.setId(item.id);
        return v;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (isVisible && event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            dismiss();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        mLastEvent = ev;
        return isVisible ? super.dispatchTouchEvent(ev) : false;
    }

    private View getViewNearLocation(int x, int y) {
        for (ArcMenuItem item : menuItems) {
            if (item.bounds.contains(x, y)) {
                return findViewById(item.id);
            }
        }
        return null;
    }

    private View previousHighlightView;
    private boolean isSecondDown;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                isSecondDown = true;
            case MotionEvent.ACTION_MOVE:
                checkHighlightView(event);
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (!confirmOnClick || isSecondDown) {
                    dismiss();
                }
                isSecondDown = false;
                return false;
        }
        return true;
    }

    private final void checkHighlightView(MotionEvent event) {
        if (isConfigured && (!confirmOnClick || isSecondDown)) {
            View v = getViewNearLocation((int) event.getRawX(), (int) event.getRawY());
            if (v != null) {
                if (v.equals(previousHighlightView)) {
                    return;
                }
                v.animate().scaleX(SCALE).scaleY(SCALE)
                        .setInterpolator(new OvershootInterpolator(1.5f)).start();
            }
            if (previousHighlightView != null) {
                previousHighlightView.animate().scaleX(1f).scaleY(1f)
                        .setInterpolator(new DecelerateInterpolator(1.5f))
                        .setDuration(225).start();
            }
            previousHighlightView = v;
        }
    }

    public final void add(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArcFrameLayout parent = (ArcFrameLayout) activity.findViewById(R.id.arc_menu_parent);
                if (parent == null) {
                    throw new NullPointerException("ArcFrameLayout not found, but you try to use ArcContextMenu... don't do that...");
                }

                WindowManager.LayoutParams params = new WindowManager.LayoutParams();
                params.width = WindowManager.LayoutParams.MATCH_PARENT;
                params.height = WindowManager.LayoutParams.MATCH_PARENT;
                params.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
                if (!confirmOnClick) {
                    params.flags |= WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
                }
                params.dimAmount = 0;
                params.format = PixelFormat.TRANSLUCENT;
                params.windowAnimations = 0;
                activity.getWindowManager().addView(ArcContextMenu.this, params);

                parent.setArcMenu(ArcContextMenu.this);
            }
        });
    }

    public void show() {
        post(new Runnable() {
            @Override
            public void run() {
                if (!isConfigured) {
                    requestLayout();
                    obtainCenterAndBounds();
                }
                if (mCenterDrawable != null) {
                    ObjectAnimator.ofInt(mCenterDrawable, "alpha", 0, 255).setDuration(350).start();
                }
                if (mHighlightBounds != null) {
                    ObjectAnimator.ofInt(ArcContextMenu.this, "highlightAlpha", 0, 255).setDuration(250).start();
                }
                ObjectAnimator animator = ObjectAnimator.ofInt(getBackground(), "alpha", 217);
                animator.setInterpolator(new DecelerateInterpolator(1.2f));
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        if (mLastEvent != null) {
                            final int childCount = getChildCount();
                            for (int i = 0; i < childCount; i++) {
                                final View v = getChildAt(i);
                                v.setX(centerX - v.getMeasuredWidth() / 2);
                                v.setY(centerY - v.getMeasuredHeight() / 2);
                                v.animate().translationX(0).translationY(0).setDuration(350)
                                        .setInterpolator(new OvershootInterpolator(1.2f)).start();
                            }
                        }
                    }
                });
                animator.start();
                isVisible = true;
            }
        });
    }

    public void dismiss() {
        if (mCenterDrawable != null) {
            ObjectAnimator.ofInt(mCenterDrawable, "alpha", 255, 0).setDuration(350).start();
        }
        if (mHighlightBounds != null) {
            ObjectAnimator.ofInt(ArcContextMenu.this, "highlightAlpha", 255, 0).setDuration(350).start();
        }
        ObjectAnimator animator = ObjectAnimator.ofInt(getBackground(), "alpha", 217, 0).setDuration(350);
        animator.setInterpolator(new AccelerateInterpolator(1.5f));
        animator.addListener(new AnimatorListenerAdapter() {
                                 @Override
                                 public void onAnimationStart(Animator animation) {
                                     super.onAnimationStart(animation);
                                     if (mLastEvent != null) {
                                         final int childCount = getChildCount();
                                         for (int i = 0; i < childCount; i++) {
                                             final View v = getChildAt(i);
                                             ViewPropertyAnimator animator = v.animate().translationX(centerX - v.getMeasuredWidth() - v.getX())
                                                     .translationY(centerY - v.getMeasuredHeight() - v.getY())
                                                     .alpha(0f)
                                                     .scaleX(1f).scaleY(1f)
                                                     .setInterpolator(new AnticipateInterpolator(1.2f));
                                             animator.setDuration(300).start();
                                         }
                                     }
                                 }

                                 @Override
                                 public void onAnimationEnd(Animator animation) {
                                     super.onAnimationEnd(animation);
                                     if (mListener != null && previousHighlightView != null) {
                                         mListener.onArcMenuItemSelect(getMenuItem(previousHighlightView.getId()));
                                     }
                                     postDelayed(new Runnable() {
                                         @Override
                                         public void run() {
                                             if (getContext() != null && getContext() instanceof Activity && isActualAttached) {
                                                 try {
                                                     ((Activity) getContext()).getWindowManager().removeView(ArcContextMenu.this);
                                                 } catch (IllegalArgumentException e) {
                                                     //ignore
                                                 }
                                             }
                                         }
                                     }, 75);
                                 }
                             }
        );
        animator.start();

        isConfigured = false;
        isVisible = false;
    }

    public final ArcMenuItem getMenuItem(int id) {
        for (ArcMenuItem item : menuItems) {
            if (item.id == id) {
                return item;
            }
        }
        return null;
    }

    public void setCenterDrawable(Drawable drawable) {
        mCenterDrawable = drawable;
        invalidate();
    }

    public static ArcContextMenu display(Activity activity, ArrayList<ArcMenuItem> items) {
        return display(activity, items, null);
    }

    public static ArcContextMenu display(Activity activity, ArrayList<ArcMenuItem> items, ArrayList<ArcHighlightRect> highlightBounds) {
        ArcContextMenu contextMenu = new ArcContextMenu(activity);
        for (ArcMenuItem item : items) {
            contextMenu.addMenuItem(item);
        }
        contextMenu.setHighlightBounds(highlightBounds);
        contextMenu.add(activity);
        contextMenu.show();
        return contextMenu;
    }

    public static class ArcMenuItem {

        private String title;
        private Drawable icon;
        private Rect bounds;
        private int id = -1;
        private int titleResource = -1;

        public ArcMenuItem() {
        }

        public ArcMenuItem title(String title) {
            this.title = title;
            return this;
        }

        public ArcMenuItem titleResource(int resource) {
            this.titleResource = resource;
            return this;
        }

        public ArcMenuItem icon(Drawable drawable) {
            this.icon = drawable;
            return this;
        }

        public ArcMenuItem id(int id) {
            this.id = id;
            return this;
        }

        public int getId() {
            return id;
        }

        public void setBounds(Rect rect) {
            bounds = rect;
        }

    }

    public static class ArcHighlightRect {

        public static ArcHighlightRect make(Rect itemRect, Rect parentRect, Rect sizeRect) {
            ArcHighlightRect rect = new ArcHighlightRect(itemRect);
            rect.inspect(parentRect, sizeRect);
            return rect;
        }

        private Rect mRect;
        private boolean isLeftVisible = true;
        private boolean isTopVisible = true;
        private boolean isRightVisible = true;
        private boolean isBottomVisible = true;

        public ArcHighlightRect(Rect rect) {
            mRect = rect;
        }

        public void inspect(Rect rect, Rect sizeRect) {
            if (mRect.width() != sizeRect.width()) {
                if (mRect.left > rect.width() / 2) {
                    isRightVisible = false;
                } else {
                    isLeftVisible = false;
                }
            }
            if (mRect.height() != sizeRect.height()) {
                if (mRect.top > rect.height() / 2) {
                    isBottomVisible = false;
                } else {
                    isTopVisible = false;
                }
            }
        }

        public void draw(Canvas canvas, Paint paint) {
            final int strokeWidth = (int) paint.getStrokeWidth() / 2;
            if (isLeftVisible) {
                canvas.drawLine(mRect.left + strokeWidth, mRect.top + strokeWidth,
                        mRect.left + strokeWidth, mRect.bottom - strokeWidth, paint);
            }
            if (isTopVisible) {
                canvas.drawLine(mRect.left + strokeWidth, mRect.top + strokeWidth,
                        mRect.right - strokeWidth, mRect.top + strokeWidth, paint);
            }
            if (isRightVisible) {
                canvas.drawLine(mRect.right - strokeWidth, mRect.top + strokeWidth,
                        mRect.right - strokeWidth, mRect.bottom - strokeWidth, paint);
            }
            if (isBottomVisible) {
                canvas.drawLine(mRect.left + strokeWidth, mRect.bottom - strokeWidth,
                        mRect.right - strokeWidth, mRect.bottom - strokeWidth, paint);
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            } else if (o instanceof Rect) {
                return o.equals(mRect);
            } else if (o instanceof ArcHighlightRect && mRect != null) {
                return mRect.equals(((ArcHighlightRect) o).mRect);
            }
            return false;
        }
    }

}
